package br.com.imobyt.funme.resources;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.imobyt.funme.models.Usuario;
import br.com.imobyt.funme.persistence.UserPersistence;

@Path("/users")
public class UserResource {

	// vamos utilizar um Map estático para
	// "simular" uma base de dados
	static private EntityManagerFactory emf;

	static {
		emf = Persistence.createEntityManagerFactory("FunmeWS");
	}

	@Path("list")
	@GET
	@Produces({ /* "text/xml", */"application/json" })
	public List<Usuario> listUsers() {

		UserPersistence up = new UserPersistence(emf.createEntityManager());

		up.beginTransaction();
		List<Usuario> c = (List<Usuario>) up.findAll(Usuario.class);
		up.close();

		return c; // new ArrayList<Usuario>(usersMap.values());
	}

	@Path("get/{id}")
	@GET
	@Produces({ "application/json" })
	public Usuario getUser(@PathParam("id") int id) {

		UserPersistence up = new UserPersistence(emf.createEntityManager());

		up.beginTransaction();
		Usuario usuario = (Usuario) up.find(Usuario.class, id);

		if (usuario == null) {
			usuario = new Usuario();
		}

		up.close();

		return usuario;
	}

	@Path("add")
	@POST
	@Consumes("application/json")
	@Produces({ "text/xml", "application/json" })
	public Usuario addUser(Usuario usuario) {
		UserPersistence up = new UserPersistence(emf.createEntityManager());

		try {
			up.beginTransaction();
			usuario = up.create(usuario);
			up.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			up.close();
		}
		return usuario;
	}

	@Path("update")
	@PUT
	@Consumes("application/json")
	@Produces({ "text/xml", "application/json" })
	public String updateUser(Usuario usuario) {
		String success = "OK";
		UserPersistence up = new UserPersistence(emf.createEntityManager());

		try {
			up.beginTransaction();
			up.update(usuario);
			up.commitTransaction();
		} catch (Exception e) {
			success = e.toString() + "\n" + usuario;
		} finally {
			up.close();
		}
		return success;
	}

	@Path("delete/{id}")
	@DELETE
	@Produces({ "text/xml", "application/json" })
	public String deleteUser(@PathParam("id") int id) {
		String success = "OK";
		UserPersistence up = new UserPersistence(emf.createEntityManager());

		try {
			up.beginTransaction();
			up.remove(id);
			up.commitTransaction();
		} catch (Exception e) {
			success = e.toString() + "\nUser ID: " + id;
		} finally {
			up.close();
		}
		return success;
	}
}
