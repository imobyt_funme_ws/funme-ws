package br.com.imobyt.funme.persistence;

import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.imobyt.funme.models.Usuario;

public class UserPersistence extends GenericPersistence {

	public UserPersistence(EntityManager em) {
		super(em);
	}

	public Usuario create(int id, String name, String password, String phone,
			String mobile, Calendar birthday, boolean sendEmail) {
		Usuario usuario = new Usuario();
		usuario.setId(id);
		usuario.setNome(name);
		usuario.setSenha(password);
		usuario.setTelefone(phone);
		usuario.setCelular(mobile);
		usuario.setAniversario(birthday.getTime());

		return create(usuario);
	}

	public Usuario create(Usuario usuario) {
		if (usuario != null) {
			usuario = em.merge(usuario);
		}

		return usuario;
	}

	public void remove(int id) {
		Usuario usuario = (Usuario) find(Usuario.class, id);

		if (usuario != null) {
			em.remove(usuario);
		}
	}

	public void update(Usuario usuario) {
		if (usuario != null) {
			em.merge(usuario);
		}
	}

}
