package br.com.imobyt.funme.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class GenericPersistence {
	protected EntityManager em;

	public GenericPersistence(EntityManager em) {
		this.em = em;
	}

	public void beginTransaction() {
		this.em.getTransaction().begin();
	}

	public void commitTransaction() {
		if (em.getTransaction().isActive())
			em.getTransaction().commit();
	}

	public void rollbackTransaction() {
		if (em.getTransaction().isActive())
			em.getTransaction().rollback();
	}

	public void close() {
		if (em.isOpen())
			em.close();
	}

	protected Object create(Object obj) {
		em.persist(obj);
		return obj;
	}

	protected void remove(Object obj) {
		if (obj != null) {
			em.remove(obj);
		}
	}

	public Object find(Class<?> c, int id) {
		return em.find(c, id);
	}

	public List<?> findAll(Class<?> c) {
		Query query = em.createQuery("SELECT o FROM " + c.getSimpleName() + " o");
		return query.getResultList();
	}

}
