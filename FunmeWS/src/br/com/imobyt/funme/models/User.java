package br.com.imobyt.funme.models;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
Classe de persistencia de usuario
teste esley 123
*/

@XmlRootElement(name="usuario")
@Entity
@Table(name="usuario")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	//@XmlElement(name="id")
	private int id;
	
	@Column(name = "nome", nullable = false)
	//@XmlElement(name="nome")
	private String name;
	
	@Column(name = "email", nullable = false)
	//@XmlElement(name="email")
	private String email;
	
	@Column(name = "senha", nullable = false)
	//@XmlElement(name="senha")
	private String password;
	
	@Column(name = "telefone", nullable = true)
	//@XmlElement(name="telefone")
	private String phone;
	
	@Column(name = "celular", nullable = false)
	//@XmlElement(name="celular")
	private String mobile;
	
	@Column(name = "aniversario", nullable = false)
	@Temporal(TemporalType.DATE)
	//@XmlElement(name="aniversario")
	private Calendar birthday;

	@Column(name = "usuario_facebook", nullable = true)
	//@XmlElement(name="usuario_facebook")
	private String facebookUser;
	
	@Column(name = "data_criacao", nullable = false)
	//@XmlElement(name="data_criacao")
	private Calendar creationTimestamp;
	
	@Column(name = "ultimo_login", nullable = true)
	//@XmlElement(name="ultimo_login")
	private Calendar lastLogin;
	
	@Column(name = "data_atualizacao", nullable = true)
	//@XmlElement(name="data_atualizacao")
	private Calendar lastUpdate;
	
	@Column(name = "latitude", nullable = true)
	//@XmlElement(name="latitude")
	private String latitude;
	
	@Column(name = "longitude", nullable = true)
	//@XmlElement(name="longitude")
	private String longitude;
	
	@Column(name = "status", nullable = false)
	//@XmlElement(name="status")
	private String status;
	
	@Column(name = "url_foto", nullable = true)
	//@XmlElement(name="url_foto")
	private String urlPhoto;
	
	private String teste1;
	private String teste2;
	

	public Calendar getBirthday() {
		return birthday;
	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public String getMobile() {
		return mobile;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	/*public List<Profile> getProfiles() {
		return profiles;
	}*/

	public void setBirthday(Calendar birthday) {
		this.birthday = birthday;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	/*public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}*/

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFacebookUser() {
		return facebookUser;
	}

	public void setFacebookUser(String facebookUser) {
		this.facebookUser = facebookUser;
	}

	public Calendar getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Calendar creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Calendar getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Calendar lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Calendar getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Calendar lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrlPhoto() {
		return urlPhoto;
	}

	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}


}
