package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the agenda database table.
 * 
 */
@Entity
@Table(name="agenda")
@NamedQuery(name="Agenda.findAll", query="SELECT a FROM Agenda a")
public class Agenda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	private String descricao;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fim;

	@Temporal(TemporalType.TIMESTAMP)
	private Date inicio;

	private String nome;

	private String telefone;

	@Column(name="url_imagem")
	private String urlImagem;

	@Column(name="url_site")
	private String urlSite;

	//bi-directional many-to-one association to Casa
	@ManyToOne
	@JoinColumn(name="id_casa")
	private Casa casa;

	//bi-directional many-to-one association to AgendaBanda
	@OneToMany(mappedBy="agenda")
	private List<AgendaBanda> agendaBandas;

	//bi-directional many-to-one association to PrecoAgenda
	@OneToMany(mappedBy="agenda")
	private List<PrecoAgenda> precoAgendas;

	public Agenda() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getFim() {
		return this.fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public Date getInicio() {
		return this.inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getUrlImagem() {
		return this.urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public String getUrlSite() {
		return this.urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public Casa getCasa() {
		return this.casa;
	}

	public void setCasa(Casa casa) {
		this.casa = casa;
	}

	public List<AgendaBanda> getAgendaBandas() {
		return this.agendaBandas;
	}

	public void setAgendaBandas(List<AgendaBanda> agendaBandas) {
		this.agendaBandas = agendaBandas;
	}

	public AgendaBanda addAgendaBanda(AgendaBanda agendaBanda) {
		getAgendaBandas().add(agendaBanda);
		agendaBanda.setAgenda(this);

		return agendaBanda;
	}

	public AgendaBanda removeAgendaBanda(AgendaBanda agendaBanda) {
		getAgendaBandas().remove(agendaBanda);
		agendaBanda.setAgenda(null);

		return agendaBanda;
	}

	public List<PrecoAgenda> getPrecoAgendas() {
		return this.precoAgendas;
	}

	public void setPrecoAgendas(List<PrecoAgenda> precoAgendas) {
		this.precoAgendas = precoAgendas;
	}

	public PrecoAgenda addPrecoAgenda(PrecoAgenda precoAgenda) {
		getPrecoAgendas().add(precoAgenda);
		precoAgenda.setAgenda(this);

		return precoAgenda;
	}

	public PrecoAgenda removePrecoAgenda(PrecoAgenda precoAgenda) {
		getPrecoAgendas().remove(precoAgenda);
		precoAgenda.setAgenda(null);

		return precoAgenda;
	}

}