package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the agenda_banda database table.
 * 
 */
@Entity
@Table(name="agenda_banda")
@NamedQuery(name="AgendaBanda.findAll", query="SELECT a FROM AgendaBanda a")
public class AgendaBanda implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AgendaBandaPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	//bi-directional many-to-one association to Agenda
	@ManyToOne
	@JoinColumn(name="id_agenda")
	private Agenda agenda;

	//bi-directional many-to-one association to Banda
	@ManyToOne
	@JoinColumn(name="id_banda")
	private Banda banda;

	public AgendaBanda() {
	}

	public AgendaBandaPK getId() {
		return this.id;
	}

	public void setId(AgendaBandaPK id) {
		this.id = id;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Agenda getAgenda() {
		return this.agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}

	public Banda getBanda() {
		return this.banda;
	}

	public void setBanda(Banda banda) {
		this.banda = banda;
	}

}