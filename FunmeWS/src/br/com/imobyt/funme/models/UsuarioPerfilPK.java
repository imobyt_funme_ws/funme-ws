package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the usuario_perfil database table.
 * 
 */
@Embeddable
public class UsuarioPerfilPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_usuario", insertable=false, updatable=false)
	private int idUsuario;

	@Column(name="id_genero", insertable=false, updatable=false)
	private int idGenero;

	public UsuarioPerfilPK() {
	}
	public int getIdUsuario() {
		return this.idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdGenero() {
		return this.idGenero;
	}
	public void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UsuarioPerfilPK)) {
			return false;
		}
		UsuarioPerfilPK castOther = (UsuarioPerfilPK)other;
		return 
			(this.idUsuario == castOther.idUsuario)
			&& (this.idGenero == castOther.idGenero);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idUsuario;
		hash = hash * prime + this.idGenero;
		
		return hash;
	}
}