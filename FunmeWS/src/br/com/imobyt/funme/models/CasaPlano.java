package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the casa_plano database table.
 * 
 */
@Entity
@Table(name="casa_plano")
@NamedQuery(name="CasaPlano.findAll", query="SELECT c FROM CasaPlano c")
public class CasaPlano implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CasaPlanoPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fim;

	@Temporal(TemporalType.TIMESTAMP)
	private Date inicio;

	//bi-directional many-to-one association to Casa
	@ManyToOne
	@JoinColumn(name="id_casa")
	private Casa casa;

	//bi-directional many-to-one association to Plano
	@ManyToOne
	@JoinColumn(name="id_plano")
	private Plano plano;

	public CasaPlano() {
	}

	public CasaPlanoPK getId() {
		return this.id;
	}

	public void setId(CasaPlanoPK id) {
		this.id = id;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getFim() {
		return this.fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public Date getInicio() {
		return this.inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Casa getCasa() {
		return this.casa;
	}

	public void setCasa(Casa casa) {
		this.casa = casa;
	}

	public Plano getPlano() {
		return this.plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

}