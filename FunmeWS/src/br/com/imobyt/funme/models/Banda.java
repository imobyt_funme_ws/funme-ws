package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the banda database table.
 * 
 */
@Entity
@Table(name="banda")
@NamedQuery(name="Banda.findAll", query="SELECT b FROM Banda b")
public class Banda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String contato;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	private String descrição;

	private String nome;

	@Column(name="url_site")
	private String urlSite;

	//bi-directional many-to-one association to AgendaBanda
	@OneToMany(mappedBy="banda")
	private List<AgendaBanda> agendaBandas;

	//bi-directional many-to-one association to Genero
	@ManyToOne
	@JoinColumn(name="id_genero")
	private Genero genero;

	public Banda() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContato() {
		return this.contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getDescrição() {
		return this.descrição;
	}

	public void setDescrição(String descrição) {
		this.descrição = descrição;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUrlSite() {
		return this.urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public List<AgendaBanda> getAgendaBandas() {
		return this.agendaBandas;
	}

	public void setAgendaBandas(List<AgendaBanda> agendaBandas) {
		this.agendaBandas = agendaBandas;
	}

	public AgendaBanda addAgendaBanda(AgendaBanda agendaBanda) {
		getAgendaBandas().add(agendaBanda);
		agendaBanda.setBanda(this);

		return agendaBanda;
	}

	public AgendaBanda removeAgendaBanda(AgendaBanda agendaBanda) {
		getAgendaBandas().remove(agendaBanda);
		agendaBanda.setBanda(null);

		return agendaBanda;
	}

	public Genero getGenero() {
		return this.genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

}