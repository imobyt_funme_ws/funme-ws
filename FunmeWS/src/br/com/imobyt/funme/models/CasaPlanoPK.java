package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the casa_plano database table.
 * 
 */
@Embeddable
public class CasaPlanoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_casa", insertable=false, updatable=false)
	private int idCasa;

	@Column(name="id_plano", insertable=false, updatable=false)
	private int idPlano;

	public CasaPlanoPK() {
	}
	public int getIdCasa() {
		return this.idCasa;
	}
	public void setIdCasa(int idCasa) {
		this.idCasa = idCasa;
	}
	public int getIdPlano() {
		return this.idPlano;
	}
	public void setIdPlano(int idPlano) {
		this.idPlano = idPlano;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CasaPlanoPK)) {
			return false;
		}
		CasaPlanoPK castOther = (CasaPlanoPK)other;
		return 
			(this.idCasa == castOther.idCasa)
			&& (this.idPlano == castOther.idPlano);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idCasa;
		hash = hash * prime + this.idPlano;
		
		return hash;
	}
}