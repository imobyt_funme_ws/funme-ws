package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the genero database table.
 * 
 */
@Entity
@Table(name="genero")
@NamedQuery(name="Genero.findAll", query="SELECT g FROM Genero g")
public class Genero implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	private String nome;

	//bi-directional many-to-one association to Banda
	@OneToMany(mappedBy="genero")
	private List<Banda> bandas;

	//bi-directional many-to-one association to Casa
	@OneToMany(mappedBy="genero")
	private List<Casa> casas;

	//bi-directional many-to-one association to Perfil
	@OneToMany(mappedBy="genero")
	private List<Perfil> perfils;

	//bi-directional many-to-one association to UsuarioPerfil
	@OneToMany(mappedBy="genero")
	private List<UsuarioPerfil> usuarioPerfils;

	public Genero() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Banda> getBandas() {
		return this.bandas;
	}

	public void setBandas(List<Banda> bandas) {
		this.bandas = bandas;
	}

	public Banda addBanda(Banda banda) {
		getBandas().add(banda);
		banda.setGenero(this);

		return banda;
	}

	public Banda removeBanda(Banda banda) {
		getBandas().remove(banda);
		banda.setGenero(null);

		return banda;
	}

	public List<Casa> getCasas() {
		return this.casas;
	}

	public void setCasas(List<Casa> casas) {
		this.casas = casas;
	}

	public Casa addCasa(Casa casa) {
		getCasas().add(casa);
		casa.setGenero(this);

		return casa;
	}

	public Casa removeCasa(Casa casa) {
		getCasas().remove(casa);
		casa.setGenero(null);

		return casa;
	}

	public List<Perfil> getPerfils() {
		return this.perfils;
	}

	public void setPerfils(List<Perfil> perfils) {
		this.perfils = perfils;
	}

	public Perfil addPerfil(Perfil perfil) {
		getPerfils().add(perfil);
		perfil.setGenero(this);

		return perfil;
	}

	public Perfil removePerfil(Perfil perfil) {
		getPerfils().remove(perfil);
		perfil.setGenero(null);

		return perfil;
	}

	public List<UsuarioPerfil> getUsuarioPerfils() {
		return this.usuarioPerfils;
	}

	public void setUsuarioPerfils(List<UsuarioPerfil> usuarioPerfils) {
		this.usuarioPerfils = usuarioPerfils;
	}

	public UsuarioPerfil addUsuarioPerfil(UsuarioPerfil usuarioPerfil) {
		getUsuarioPerfils().add(usuarioPerfil);
		usuarioPerfil.setGenero(this);

		return usuarioPerfil;
	}

	public UsuarioPerfil removeUsuarioPerfil(UsuarioPerfil usuarioPerfil) {
		getUsuarioPerfils().remove(usuarioPerfil);
		usuarioPerfil.setGenero(null);

		return usuarioPerfil;
	}

}