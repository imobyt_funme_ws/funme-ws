package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the plano database table.
 * 
 */
@Entity
@Table(name="plano")
@NamedQuery(name="Plano.findAll", query="SELECT p FROM Plano p")
public class Plano implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	private String descricao;

	private String nome;

	private BigDecimal valor;

	//bi-directional many-to-one association to CasaPlano
	@OneToMany(mappedBy="plano")
	private List<CasaPlano> casaPlanos;

	public Plano() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public List<CasaPlano> getCasaPlanos() {
		return this.casaPlanos;
	}

	public void setCasaPlanos(List<CasaPlano> casaPlanos) {
		this.casaPlanos = casaPlanos;
	}

	public CasaPlano addCasaPlano(CasaPlano casaPlano) {
		getCasaPlanos().add(casaPlano);
		casaPlano.setPlano(this);

		return casaPlano;
	}

	public CasaPlano removeCasaPlano(CasaPlano casaPlano) {
		getCasaPlanos().remove(casaPlano);
		casaPlano.setPlano(null);

		return casaPlano;
	}

}