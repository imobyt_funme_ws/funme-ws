package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the agenda_banda database table.
 * 
 */
@Embeddable
public class AgendaBandaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_agenda", insertable=false, updatable=false)
	private int idAgenda;

	@Column(name="id_banda", insertable=false, updatable=false)
	private int idBanda;

	public AgendaBandaPK() {
	}
	public int getIdAgenda() {
		return this.idAgenda;
	}
	public void setIdAgenda(int idAgenda) {
		this.idAgenda = idAgenda;
	}
	public int getIdBanda() {
		return this.idBanda;
	}
	public void setIdBanda(int idBanda) {
		this.idBanda = idBanda;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AgendaBandaPK)) {
			return false;
		}
		AgendaBandaPK castOther = (AgendaBandaPK)other;
		return 
			(this.idAgenda == castOther.idAgenda)
			&& (this.idBanda == castOther.idBanda);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idAgenda;
		hash = hash * prime + this.idBanda;
		
		return hash;
	}
}