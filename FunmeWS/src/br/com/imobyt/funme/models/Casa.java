package br.com.imobyt.funme.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the casa database table.
 * 
 */
@Entity
@Table(name="casa")
@NamedQuery(name="Casa.findAll", query="SELECT c FROM Casa c")
public class Casa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	private Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	private Date dataCriacao;

	private String descricao;

	private String email;

	private String endereco;

	private String latitude;

	private String longitude;

	private String nome;

	@Column(name="nome_contato")
	private String nomeContato;

	private String telefone;

	@Column(name="url_imagem")
	private String urlImagem;

	@Column(name="url_site")
	private String urlSite;

	//bi-directional many-to-one association to Agenda
	@OneToMany(mappedBy="casa")
	private List<Agenda> agendas;

	//bi-directional many-to-one association to Genero
	@ManyToOne
	@JoinColumn(name="id_genero")
	private Genero genero;

	//bi-directional many-to-one association to CasaPlano
	@OneToMany(mappedBy="casa")
	private List<CasaPlano> casaPlanos;

	public Casa() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataAtualizacao() {
		return this.dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeContato() {
		return this.nomeContato;
	}

	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getUrlImagem() {
		return this.urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public String getUrlSite() {
		return this.urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public List<Agenda> getAgendas() {
		return this.agendas;
	}

	public void setAgendas(List<Agenda> agendas) {
		this.agendas = agendas;
	}

	public Agenda addAgenda(Agenda agenda) {
		getAgendas().add(agenda);
		agenda.setCasa(this);

		return agenda;
	}

	public Agenda removeAgenda(Agenda agenda) {
		getAgendas().remove(agenda);
		agenda.setCasa(null);

		return agenda;
	}

	public Genero getGenero() {
		return this.genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public List<CasaPlano> getCasaPlanos() {
		return this.casaPlanos;
	}

	public void setCasaPlanos(List<CasaPlano> casaPlanos) {
		this.casaPlanos = casaPlanos;
	}

	public CasaPlano addCasaPlano(CasaPlano casaPlano) {
		getCasaPlanos().add(casaPlano);
		casaPlano.setCasa(this);

		return casaPlano;
	}

	public CasaPlano removeCasaPlano(CasaPlano casaPlano) {
		getCasaPlanos().remove(casaPlano);
		casaPlano.setCasa(null);

		return casaPlano;
	}

}