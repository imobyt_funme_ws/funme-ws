drop table funme.agenda_banda;
drop table funme.preco_agenda;
drop table funme.agenda;
drop table funme.casa_plano;
drop table funme.casa;
drop table funme.plano;
drop table funme.usuario_perfil;
drop table funme.usuario;
drop table funme.perfil;
drop table funme.banda;
drop table funme.genero;

/*--------------------------------tabela de genero------------------------------------------------------*/

create table funme.genero (
id int(9) auto_increment primary key,
nome varchar(60) not null,
data_criacao datetime not null default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp
);

/*--------------------------------tabela de banda------------------------------------------------------*/

create table funme.banda (
id int(9) auto_increment primary key,
nome varchar(60) not null,
descrição varchar(500) not null,
url_site varchar(20),
contato varchar(20),
id_genero int(9),
data_criacao datetime not null default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_genero) references funme.genero(id)
);

/*--------------------------------tabela de perfil------------------------------------------------------*/

create table funme.perfil (
id int(9) auto_increment primary key,
nome varchar(60) not null,
descrição varchar(500),
id_genero int(9),
data_criacao datetime not null default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_genero) references funme.genero(id)
);

/*--------------------------------tabela de usuario------------------------------------------------------*/

create table funme.usuario (
id int(9) auto_increment primary key,
nome varchar(60) not null,
email varchar(100) not null,
telefone varchar(20),
celular varchar(20) not null, 
aniversario date not null,
senha varchar(16) not null,
usuario_facebook varchar(16),
data_criacao datetime not null default current_timestamp,
ultimo_login datetime,
data_atualizacao datetime default current_timestamp on update current_timestamp,
latitude varchar(20) ,
longitude varchar(20),
status varchar(1) not null,
url_foto varchar(200)
);

/*--------------------------------tabela de usuario_perfil------------------------------------------------------*/

create table funme.usuario_perfil (
id_usuario int(9) not null,
id_genero int(9) not null,
data_criacao datetime  not null default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_usuario) references funme.usuario(id),
foreign key (id_genero) references funme.genero(id),
primary key (id_usuario, id_genero)
);


/*--------------------------------tabela de plano------------------------------------------------------*/

create table funme.plano (
id int(9) auto_increment primary key,
nome varchar(30) not null,
descricao varchar(200) not null,
valor decimal not null default 0,
data_criacao datetime default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp
);

/*--------------------------------tabela de casa------------------------------------------------------*/

create table funme.casa (
id int(9) auto_increment primary key,
nome varchar(30) not null,
descricao varchar(200) not null,
endereco varchar(200) not null,
latitude varchar(50) not null,
longitude varchar(50) not null,
url_site varchar(200),
nome_contato varchar(200),
email varchar(100),
telefone varchar(20),
url_imagem varchar(200),
id_genero int(9) not null,
data_criacao datetime default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_genero) references funme.genero(id)
);

/*--------------------------------tabela de casa_plano------------------------------------------------------*/

create table funme.casa_plano (
id_casa int(9) not null,
id_plano int(9) not null,
inicio datetime default current_timestamp,
fim datetime default current_timestamp,
data_criacao datetime  not null default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_casa) references funme.casa(id),
foreign key (id_plano) references funme.plano(id),
primary key (id_casa, id_plano)
);

/*--------------------------------tabela de agenda------------------------------------------------------*/

create table funme.agenda (
id int(9) auto_increment primary key,
id_casa int(9) not null,
inicio datetime default current_timestamp,
fim datetime default current_timestamp,
nome varchar(30) not null,
descricao varchar(200) not null,
url_imagem varchar(200),
url_site varchar(200),
telefone varchar(20),
data_criacao datetime default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_casa) references funme.casa(id)
);

/*--------------------------------tabela de preco agenda------------------------------------------------------*/

create table funme.preco_agenda (
id int(9) auto_increment primary key,
descricao varchar(200) not null,
valor decimal not null default 0,
inicio datetime default current_timestamp,
fim datetime default current_timestamp,
status varchar(1) not null,
id_agenda int(9) not null,
data_criacao datetime default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_agenda) references funme.agenda(id)
);

/*--------------------------------tabela de agenda_banda------------------------------------------------------*/

create table funme.agenda_banda (
id_agenda int(9) not null,
id_banda int(9) not null,
data_criacao datetime  not null default current_timestamp,
data_atualizacao datetime default current_timestamp on update current_timestamp,
foreign key (id_agenda) references funme.agenda(id),
foreign key (id_banda) references funme.banda(id),
primary key (id_agenda, id_banda)
);


